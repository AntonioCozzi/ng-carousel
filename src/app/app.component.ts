import { Component, OnInit } from '@angular/core';
import { ImageService } from './image.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  position: number = 0
  data = []

  constructor(private imageService: ImageService) {}

  ngOnInit(): void {
    this.imageService.getImages()
      .subscribe(response => this.data = response)

    setInterval(() => this.next(), 3000)
  }

  next() {
    this.position < this.data.length - 1 ?
      this.position++
      :
      this.position = 0
  }

  previous() {
    this.position > 0 ? this.position-- : this.position = this.data.length - 1
  }
}
