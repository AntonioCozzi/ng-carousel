describe("First render", () => {
    it("should visit login page", () => {
        cy.server()
        cy.route({
            method: 'GET',
            url: '/v2/list',
        }).as('getPhotos')

        cy.visit("http://localhost:4200/");

        cy.wait('@getPhotos').then((xhr) => {
            assert.isNotNull(xhr.response.body, 'Photos loaded.')
        })
    });
});

describe("Should render components", () => {
    it("should render the image", () => {
        cy.get('#image').should('be.visible');
    })

    it("Should render buttons", () => {
        cy.get('#next').should('be.visible');
        cy.get('#prev').should('be.visible');
    })
})

describe("Interact with page's components", () => {
    it("Should click next button", () => {
        cy.get('#next').click();
    })

    it("Should click previous button", () => {
        cy.get('#prev').click();
    })

    it("Should image change every 3 seconds", () => {   
        cy.get('#image').invoke('attr', 'src').then((firstSrc) => {
            const src1 = firstSrc

            cy.wait(3000);

            cy.get('#image').invoke('attr', 'src').then((nextSrc) => {
                expect(nextSrc).to.not.equal(src1)
            });
        });
    })
})

